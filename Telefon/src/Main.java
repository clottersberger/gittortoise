/**
 * Eine Smartphoneapp in Java.
 * Dies ist ein Javadoc-Kommentar.
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 30.09.2020
 */

//Klasse Main
public class Main {

    public static void main(String[] args) {

        //Objekt erstellen
        SamsungGalaxyS20 bestesGalaxyS20 = new SamsungGalaxyS20(850, "Smartphone Galaxy S20", "schwarz");
        //Ausgabe
        System.out.println("Preis: " + bestesGalaxyS20.getPreis() + "€");
        System.out.println("Produkt: " + bestesGalaxyS20.getProduktTyp());
        System.out.println("Farbe: " + bestesGalaxyS20.getFarbe());
        System.out.println();

        //Methoden aufrufen
        bestesGalaxyS20.powerOn();
        bestesGalaxyS20.anrufen();
        bestesGalaxyS20.esKlingelt();
        System.out.println();

        //Objekt erstellen
        SamsungGalaxyS10 chrisGalaxyS10 = new SamsungGalaxyS10(550, "Smartphone Galaxy S10", "blau");
        //Ausgabe
        System.out.println("Preis: " + chrisGalaxyS10.getPreis() + "€");
        System.out.println("Produkt: " + chrisGalaxyS10.getProduktTyp());
        System.out.println("Farbe: " + chrisGalaxyS10.getFarbe());
        System.out.println();

        //Methoden aufrufen
        chrisGalaxyS10.powerOn();
        chrisGalaxyS10.anrufen();
        chrisGalaxyS10.esKlingelt();
    }
}

