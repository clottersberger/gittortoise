/**
 * Eine Smartphoneapp in Java.
 * Dies ist ein Javadoc-Kommentar.
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 30.09.2020
 */

//Abstrakte Klasse
public abstract class Samsung {

    //Eigenschaften/Attribute
    private double preis;
    private String produktTyp;

    //Konstruktoren
    /**
     *
     * @param preis Der Preis des Smartphones
     * @param produktTyp Der Produkttyp z. B. Smartphone
     */
    public Samsung(double preis, String produktTyp) {
        this.preis = preis;
        this.produktTyp = produktTyp;
    }

    //Methoden
    /**
     *
     * @return Liefert den Preis
     */
    public double getPreis() {
        return preis;
    }

    /**
     *
     * @param preis Setzt den Preis
     */
    public void setPreis(double preis) {
        this.preis = preis;
    }

    /**
     *
     * @return Liefert den Produkttyp
     */
    public String getProduktTyp() {
        return produktTyp;
    }

    /**
     *
     * @param produktTyp Setzt den Produkttyp
     */
    public void setProduktTyp(String produktTyp) {
        this.produktTyp = produktTyp;
    }
}

