/**
 * Eine Smartphoneapp in Java.
 * Dies ist ein Javadoc-Kommentar.
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 30.09.2020
 */

//Klasse
public class SamsungGalaxyS20 extends Samsung implements ITelefon{

    //Eigenschaften/Attribute
    private String farbe;

    //Konstruktoren
    /**
     *
     * @param preis Der Preis
     * @param produktTyp Der Produkttyp
     * @param farbe Die Farbe
     */
    public SamsungGalaxyS20(double preis, String produktTyp, String farbe) {
        super(preis, produktTyp);
        this.farbe = farbe;
    }

    //Methoden
    @Override
    public void powerOn() {
        System.out.println("Das Telefon SamsungGalaxyS20 wurde gestartet.");
    }

    /**
     *
     * @return Liefert, ob das Smartphone klingelt
     */
    @Override
    public boolean esKlingelt() {
        System.out.println("Das Telefon SamsungGalaxyS20 klingelt.");
        return false;
    }

    /**
     *
     * @return Liefert, ob das Smartphone angerufen wird
     */
    @Override
    public boolean anrufen() {
        System.out.println("Das Telefon SamsungGalaxyS20 wird angerufen.");
        return false;
    }

    /**
     *
     * @return Liefert die Farbe
     */
    public String getFarbe() {
        return farbe;
    }

    /**
     *
     * @param farbe Setzt die Farbe
     */
    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }
}
