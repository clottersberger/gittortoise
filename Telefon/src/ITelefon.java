/**
 * Eine Smartphoneapp in Java.
 * Dies ist ein Javadoc-Kommentar.
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 30.09.2020
 */

//Interface
public interface ITelefon {

    //Methodensignaturen/Methodenköpfe
    public abstract void powerOn();

    /**
     *
     * @return Wird geliefert, wenn das Smartphone klingelt
     */
    public abstract boolean esKlingelt();

    /**
     *
     * @return Wird geliefert, wenn man angerufen wird
     */
    public abstract boolean anrufen();
}
